
app_api_management
============

This package provides convenient functions on top of the *app_api_management* module.

## Usage

```js
var appApiMgmt = require('app_api_management');
```

## API

### validateRequest(config, callback)

Creates a connection to a HANA database:

```js
var config = {
    appName     : "XXX",
    pathName     : "XXX",
    environment     : "XXX",
    ipAddress : "XXX",
    throttleCount : "XXX",
    enabled : "XXX",
    hostDNS : "XXX",
    procedureName: "XXX",
    services : "XXX"
  };

appApiMgmt.validateRequest(config, function(error, status) {
  if (error || !status) {
    return console.error(error);
  } else {
    console.log(status); // true
  }
});
```
